��          �      �        "   	     ,     :     J     W  	   ]     g     p     �  !   �  -   �     �     �       W   
  3   b  "   �  %   �     �     �       :     #  N  2   r     �     �     �     �  
   �     �     �     
  +     ,   J     w     �     �  a   �  @      $   A  "   f     �     �     �  G   �                                                                         	                           
    Append Open Graph and Twitter tags Default image Derivative type Image to use Large Meta Tags No media Open Graph type Resource class Resource thumbnail or first image Resource thumbnail or selected asset for page Selected asset below Simple og:type Square The module "%s" was automatically deactivated because the dependencies are unavailable. This module has resources that connot be installed. This module requires modules "%s". This module requires the module "%s". Twitter creator account Twitter domain Twitter site When a thumbnail exists, the size is not taken in account. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-03-01 00:00+0000
Last-Translator: Daniel Berthereau <Daniel.fr@Berthereau.net>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
 Ajouter les éléments méta Open Graph et Twitter Image par défaut Fchiers dérivés Image à utiliser Large Méta tags Pas de média Type Open Graph Classe de ressource Vignette de la ressource ou première image Vignette de la ressource ou image ci-dessous Image ci-dessous Simple og:type Carré Le module "%s" a été automatiquement désactivé car ses dépendances ne sont plus disponibles. Ce module a des ressources qui ne peuvent pas être installées. Ce module requiert les modules "%s". Ce module requiert le module "%s". Compte auteur Twitter Domaine Twitter Site Twitter Quand une vignette est définie, la taille n’est pas prise en compte. 